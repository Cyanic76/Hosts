# Other lists

Only use them if you know what you do.

> You may use those lists as references, as per the MIT license included in this project.

| List | Link | Description |
|-|-|-|
| Chocolatey | [txt](https://hosts.cyanic.me/kind/Safe/chocolatey.txt) | Windows package manager |
| Cloudflare | [txt](https://hosts.cyanic.me/kind/Safe/cloudflare.txt) | This **WILL** break a lot of websites and apps |
| Humansecurity | [txt](https://hosts.cyanic.me/kind/Safe/humansecurity.txt) | HumanSecurity |
| Zoho | [txt](https://hosts.cyanic.me/kind/Safe/zoho.txt) | Zoho Domains |
