# Google adlist

Since pretty much every Google website uses `consent.google.*` to get your consent about ads, cookies and tracking, and that this subdomain is blocked, this will break access to a lot of Google websites.

## Android services

- Basic features
- Clock Sync via NTP
- Connectivity Check (your phone will report not connected to Internet)
- Contact sync via Google Account
- Google TV
- Parts of AMP
- Personal Safety
- Some WearOS features

## Google Apps

- Android Developers
- Arts & Culture
- AR/VR
- Assistant
- Blogspot and Blogger
- Books
- Calendar
- Chat, Duo, Groups, Hangouts, Meet and Voice
- Classroom
- Cloud
- Contacts
- Docs, Sheets, Slides, Drive, Keep, Sites and Forms
- Fi
- Fonts
- Mail, including SMTP/IMAP/POP
- Maps, Earth and Street View
- News
- Password Manager
- Pay (contactless payments)
- Photos
- Search, Image Search
- Support and Answers
- Translate
- YouTube

## Other Google services

- Accounts
- Addons
- AdMob, Ads, AdSense, AdWords, Analytics, Crashlytics, Doubleclick and Fastclick
- [Ajax APIs](https://support.google.com/code/answer/55728)
- Autofill, Search Suggestions
- [BigQuery](https://cloud.google.com/bigquery/docs/introduction)
- Chat feature in Android Messages app
- Chrome Addons
- [Domains](https://domains.google)
- Firebase
- Fonts
- Google Developers
- Goo.gl links
- [Policies](https://policies.google.com) (Privacy/Terms of Use)
- Waze

## Other Google projects

- [Coral](https://developers.googleblog.com/2019/03/introducing-coral-our-platform-for.html)
- [Flutter](https://flutter.dev)
- Googleplex
- IDX.dev
