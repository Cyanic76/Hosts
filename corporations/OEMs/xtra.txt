! Title: Qualcomm/XTRA - Cyanic Hosts
! Expires: 1 month
! Description: A list of Qualcomm and XTRA domains.
! Homepage: https://hosts.cyanic.me
! Source: https://hosts.cyanic.me/corporations/OEMs/xtra.txt

0.0.0.0 alexa-global-in.qualcomm.com
0.0.0.0 alexa-in-lv.qualcomm.com
0.0.0.0 alexa-in-sd.qualcomm.com
0.0.0.0 apigwx-aws.qualcomm.com
0.0.0.0 cne-ssl.qualcomm.com
0.0.0.0 cne.qualcomm.com
0.0.0.0 corpawswsgw-int.qualcomm.com
0.0.0.0 corpawswsgwx.qualcomm.com
0.0.0.0 createpoint.qti.qualcomm.com
0.0.0.0 cvpn-blr.ras.qualcomm.com
0.0.0.0 cvpn-hyd.ras.qualcomm.com
0.0.0.0 cvpn-lv.ras.qualcomm.com
0.0.0.0 ext.qclogin.qualcomm.com
0.0.0.0 gslb.qualcomm.com
0.0.0.0 mail.qualcomm.com
0.0.0.0 mx.qti.qualcomm.com
0.0.0.0 qcc.qccgeo.qualcomm.com
0.0.0.0 qcc.qualcom.com
0.0.0.0 qcc.qualcomm.com
0.0.0.0 qclogin.qualcomm.com
0.0.0.0 qdma.qualcomm.com
0.0.0.0 qti.qualcomm.com
0.0.0.0 qualcomm.com
0.0.0.0 ras.qualcomm.com
0.0.0.0 statmanq.qualcomm.com
0.0.0.0 tls.telemetry.swe.quicinc.com
0.0.0.0 vip-drekar-cas-lvdmzprd.qualcomm.com
0.0.0.0 www.qualcomm.com

# This WILL break GPS functionality on Qualcomm phones.
# XTRA is hosted on Amazon.

0.0.0.0 18.239.199.21
0.0.0.0 18.239.199.44
0.0.0.0 35.90.24.44
0.0.0.0 52.35.146.2
0.0.0.0 54.214.105.96
0.0.0.0 65.8.161.92
0.0.0.0 99.84.238.128
0.0.0.0 108.139.10.41
0.0.0.0 108.139.10.52
0.0.0.0 108.139.10.83

0.0.0.0 cds.gtp.izatcloud.net
0.0.0.0 cds.gtpstage.izatcloud.net
0.0.0.0 cfnts.xboxtest.izatcloud.net
0.0.0.0 gpstime.xboxprod.gpsonextra.net
0.0.0.0 gtp-tst-new.izatcloud.net
0.0.0.0 gtp1.gtp.izatcloud.net
0.0.0.0 gtp1_weighted.gtp.izatcloud.net
0.0.0.0 gtp1.izatcloud.net
0.0.0.0 gtp2.izatcloud.net
0.0.0.0 gtpa1.izatcloud.net
0.0.0.0 gtpc1.izatcloud.net
0.0.0.0 gtpcn.izatcloud.net
0.0.0.0 gtpcn1.izatcloud.net
0.0.0.0 iot1.xboxprod.xtracloud.net
0.0.0.0 iot2.xboxprod.xtracloud.net
0.0.0.0 iot3.xboxprod.xtracloud.net
0.0.0.0 meena-test.xboxstage.izatcloud.net
0.0.0.0 mycfnts.xboxtest.izatcloud.net
0.0.0.0 myntpsec.xboxtest.izatcloud.net
0.0.0.0 nts.xboxprod.xtracloud.net
0.0.0.0 nts.xboxtest.xtracloud.net
0.0.0.0 path1.xboxtest.izatcloud.net
0.0.0.0 path1.xtracloud.net
0.0.0.0 path2.xboxtest.izatcloud.net
0.0.0.0 path2.xtracloud.net
0.0.0.0 path4.xboxprod.xtracloud.net
0.0.0.0 path5.xboxprod.xtracloud.net
0.0.0.0 path6.xboxprod.xtracloud.net
0.0.0.0 path7.xboxprod.xtracloud.net
0.0.0.0 rish3.xboxtest.izatcloud.net
0.0.0.0 s2s.izatcloud.net
0.0.0.0 s2s.xbox.izatcloud.net
0.0.0.0 s2s.xboxprod.izatcloud.net
0.0.0.0 ssl-test.izatcloud.net
0.0.0.0 time.izatcloud.net
0.0.0.0 time.xboxprod.izatcloud.net
0.0.0.0 time.xtracloud.net
0.0.0.0 uds.poc.izatcloud.net
0.0.0.0 udsnonma.poc.izatcloud.net
0.0.0.0 wnro.test.izatcloud.net
0.0.0.0 wnrotest.izatcloud.net
0.0.0.0 www.izatcloud.net
0.0.0.0 xboxprod.xtracloud.net
0.0.0.0 xp1.gpsonextra.net
0.0.0.0 xp2.gpsonextra.net
0.0.0.0 xtra1.gpsonextra.net
0.0.0.0 xtra2.gpsonextra.net
0.0.0.0 xtra3.gpsonextra.net
0.0.0.0 xtra4.gpsonextra.net
0.0.0.0 xtra5.gpsonextra.net
0.0.0.0 xtra6.gpsonextra.net
0.0.0.0 xtracloud.net
0.0.0.0 xtrapath.xtracloud.net
0.0.0.0 xtrapath1.izatcloud.net
0.0.0.0 xtrapath2.izatcloud.net
0.0.0.0 xtrapath3.izatcloud.net
0.0.0.0 xtrapath1.qcomgeo2.com
0.0.0.0 xtrapath1.xboxprod.izatcloud.net
0.0.0.0 xtratime.xboxprod.izatcloud.net
