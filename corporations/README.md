# Corporations

| List | Link | Description |
|-|-|-|
| Adobe | [txt](https://hosts.cyanic.me/corporations/adobe.txt) | Adobe |
| Apple | [txt](https://hosts.cyanic.me/corporations/OEMs/apple.txt) | Apple and iCloud services |
| Avast | [txt](https://hosts.cyanic.me/corporations/avast.txt) | Avast antivirus, telemetry and services ([highly recommended](https://www.ftc.gov/business-guidance/blog/2024/02/ftc-says-avast-promised-privacy-pirated-consumers-data-treasure)) |
| AVG | [txt](https://hosts.cyanic.me/corporations/avg.txt) | AVG |
| Disqus | [txt](https://hosts.cyanic.me/corporations/disqus.txt) | Disqus |
| Google | [txt](https://hosts.cyanic.me/corporations/Google/google.txt) | Google, Android and other Google telemetry and services |
| HP | [txt](https://hosts.cyanic.me/corporations/OEMs/hp.txt) | HP |
| Huawei | [txt](https://hosts.cyanic.me/corporations/OEMs/hicloud.txt) | Huawei, HiCloud and associated services |
| iAdvize | [txt](https://hosts.cyanic.me/corporations/iadvize.txt) | iAdvize |
| McAfee | [txt](https://hosts.cyanic.me/corporations/mcafee.txt) | McAfee anti-virus |
| Microsoft | [txt](https://hosts.cyanic.me/corporations/Microsoft/ms.txt) | Microsoft, Windows, Xbox and more |
| Microsoft Office | [txt](https://hosts.cyanic.me/corporations/Microsoft/office.txt) | Word, Excel... |
| Nvidia | [txt](https://hosts.cyanic.me/corporations/Other/nvidia.txt) | Nvidia |
| OpenAI | [txt](https://hosts.cyanic.me/corporations/AI/openai.txt) | OpenAI, ChatGPT |
| Xiaomi | [txt](https://hosts.cyanic.me/corporations/OEMs/xiaomi.txt) | Xiaomi, including MIUI |
| Xtra | [txt](https://hosts.cyanic.me/corporations/OEMs/xtra.txt) | GPS on Qualcomm phones |

## Gaming

| List | Link | Description |
|-|-|-|
| Epic | [txt](https://hosts.cyanic.me/corporations/Games/epicgames.txt) | Epic Games |
| Riot | [txt](https://hosts.cyanic.me/corporations/Games/riot.txt) | Riot Games, see #8 |
| Roblox | [txt](https://hosts.cyanic.me/corporations/Games/roblox.txt) | Roblox |
| Steam | [txt](https://hosts.cyanic.me/corporations/Games/steam.txt) | Steam |

## Online shopping

| List | Link | Description |
|-|-|-|
| Alibaba/AliExpress | [txt](https://hosts.cyanic.me/corporations/Shopping/alibaba_aliexpress.txt) | Alibaba and AliExpress |
| Amazon | [txt](https://hosts.cyanic.me/corporations/Shopping/amazon.txt) | Amazon, Kindle & more |
| Ebay | [txt](https://hosts.cyanic.me/corporations/Shopping/ebay.txt) | Ebay |
| Leboncoin | [txt](https://hosts.cyanic.me/corporations/Shopping/leboncoin.txt) | Le Bon Coin (french platform) |
| Stripe | [txt](https://hosts.cyanic.me/corporations/Shopping/stripe.txt) | Payment and identity processor |

## Social

| List | Link | Description |
|-|-|-|
| Discord | [txt](https://hosts.cyanic.me/corporations/Social/discord.txt) | Discord (hosted on Google) |
| Facebook | [txt](https://hosts.cyanic.me/corporations/Social/facebook.txt) | Facebook and Instagram |
| Kaltura | [txt](https://hosts.cyanic.me/corporations/Social/kaltura.txt) | Kaltura |
| Medium | [txt](https://hosts.cyanic.me/corporations/Social/medium.txt) | Medium |
| Reddit | [txt](https://hosts.cyanic.me/corporations/Social/reddit.txt) | Reddit |
| Threads | [txt](https://hosts.cyanic.me/corporations/Social/threads.txt) | Threads |
| TikTok | [txt](https://hosts.cyanic.me/corporations/Social/tiktok.txt) | TikTok (formerly Musical.ly) |
| Vimeo | [txt](https://hosts.cyanic.me/corporations/Social/vimeo.txt) | Vimeo |
| WhatsApp | [txt](https://hosts.cyanic.me/corporations/Social/whatsapp.txt) | Meta's messaging app |
| X | [txt](https://hosts.cyanic.me/corporations/Social/twitter.txt) | X (formerly Twitter) |

## Website breaker

| List | Link | Description |
|-|-|-|
| Typekit | [txt](https://hosts.cyanic.me/corporations/Other/typekit.txt) | Typekit |
