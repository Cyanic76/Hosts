<center>
  <img src="src/assets/banner.png">
  <br>
  Get rid of those unnecessary ads, telemetry and companies you don't like.
  <br><br>
  <img src="https://img.shields.io/badge/-Codeberg%20Pages-339933?style=flat&logo=javascript&logoColor=white&label=Hosted%20on" alt="Codeberg ad">
  <img src="https://img.shields.io/badge/-MIT-333333?style=flat&label=License" alt="MIT License">
  <a href="https://cyanic.me/discord"><img src="https://img.shields.io/badge/Matrix%20room-a?color=0DBD8B&label=Join+the&logoColor=ffffff"></a>
  <img src="https://ci.codeberg.org/api/badges/9346/status.svg" alt="Pipeline status">
</center>

<br>

# About

The [main list](https://hosts.cyanic.me/cyanicHosts.txt) includes **8.4k**+ telemetry, advertisements, annoyances and other unnecessary hosts.

| List | Link | Description |
|-|-|-|
| Main | [txt](https://hosts.cyanic.me/cyanicHosts.txt) | The main list. |

## Releases

Last update: **1 March 2025**.

> Starting from **1 Sep 2024**, you may receive one update per month, unless there are pull requests in-between.

# You choose what to block.

This project includes a basic and general-purpose hosts list, **40+** lists about corporations and more.

See the `.gitignore` file to know what's coming up.

[Get the links](https://codeberg.org/Cyanic76/Hosts/src/branch/pages/corporations/README.md) to the corp lists.

[Read this](https://r.cyanic.me/answer/hosts/3.html) if you're manually importing my hosts files to your devices.

## Other

| List | Link | Description |
|-|-|-|
| Cookie banners | [txt](https://hosts.cyanic.me/kind/cookie_banners.txt) | You should use this list. |
| Quizlet | [txt](https://hosts.cyanic.me/corporations/quizlet.txt) | Quizlet is a learning website. |

# Contributing

You're welcome to request new hosts to be added to the lists!

If a host added to the main list **breaks a website**, feel free to report such issues.

The <a href="https://codeberg.org/Cyanic76/Hosts/wiki/Contributing">Contributor's Guide</a> applies. ***Will be updated on 1 Apr 2025!***

<a href="https://www.defectivebydesign.org/drm-free"><img src="https://static.fsf.org/dbd/label/DRM-free%20label%20120.en.png" alt="DefectiveByDesign.org - All files are DRM-free!" width="50" height="50" border="0" align="middle" /></a>

[Join the discussion](https://cyanic.me/discord).

---

Maintained by [Cyanic76](https://cyanic.me), licensed under [MIT](https://codeberg.org/Cyanic76/Hosts/raw/branch/pages/LICENSE).

*Any and all trademarks are the property of their respective owners.*
