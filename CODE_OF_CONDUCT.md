# Code of Conduct

Version 2.0.1 from 2025-02-26 by Cyanic76.

> *This version document will take effect on 2025-04-01.*
> *Check the [Blame](https://codeberg.org/Cyanic76/Hosts/blame/branch/pages/CODE_OF_CONDUCT.md) to see what's new. Use the commit history on this file to retrieve the previous version (2.0.0).*

---

This project has adopted the [Cyanic's Code of Conduct](https://codeberg.org/Cyanic76/pages/src/branch/pages/CODE_OF_CONDUCT.md).

- As per the 6th section in the above document, this document includes a set of guidelines specific to this project in addition the Cyanic's Code of Conduct.
- As per the 7th section in the above document, all updates to this document will be announced in advance.

## 1. New domains

When asking for new domains to be added in a Issue or Pull Request, said Issue or Pull Request **may include duplicates** that will later be removed.

## 2. Removals

This only apply to the **main hosts file**.

When asking for a host removal, you should provide the name of an app or mutiple apps currently broken.

## 3. "All hosts"

While creating a Pull Request on this feature **is allowed**, creating new Issues **is not**.

By creating new Issues on this feature, you automatically become subject to the guidelines mentioned in the [5th section](https://codeberg.org/Cyanic76/pages/src/branch/pages/CODE_OF_CONDUCT.md#5-termination) of the main document.
